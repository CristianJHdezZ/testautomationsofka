# language: en

  Feature: Aplicación de serenity para programar reuniones
  This feature requires creating a business unit
  and scheduling a meeting with it.



   @Regresion
   Scenario Outline: schedule meeting
    Given it is required to enter the serenity demo page and log in with "<user>" and "<password>"
    When a new business unit is created with the data "<name>" "<parent_unit>"
    And schedule a new meeting with the data
      |nameMeeting|meetingType|meetingNumber|starDate|startHour|endDate|endHour|location|unit|organizaBy |reporter|invited1|invited2|invited3|
      |<nameMeeting>|<meetingType>|<meetingNumber>|<starDate>|<startHour>|<endDate>|<endHour>|<location>|<unit>|<organizaBy>|<reporter>|<invited1>|<invited2>|<invited3>|
    Then validate that the meeting is created with name "<nameMeeting>"
     Examples:
      |user |password|name|parent_unit|nameMeeting|meetingType|meetingNumber|starDate|startHour|endDate|endHour|location|unit|organizaBy|reporter|invited1|invited2|invited3|
      |admin|serenity|computer|Acme Corp.|test_meeting|Customer|154879|04/18/2022|11:45|04/18/2022|12:45|SD-01|computer|Natalie Russell|Aaron Taylor|Adam Stewart|Adam Edwards|Kyle Rivera|
