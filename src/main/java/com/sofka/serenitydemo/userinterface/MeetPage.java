package com.sofka.serenitydemo.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MeetPage {

    public static final Target BUTTON_MENU_MEET = Target.the("").located(By.xpath("//*[@id='nav_menu1_3']/li[2]/a/span"));
    public static final Target BUTTON_MENU_MEET_ITEM = Target.the("").located(By.xpath("//*[@id='nav_menu1_3_2']/li[1]/a/span"));
    public static final Target BUTTON_NEW_MEET = Target.the("").located(By.xpath("//*[@id='GridDiv']/div[2]/div[2]/div[1]/span"));
    public static final Target INPUT_NAME_MEET = Target.the("").located(By.xpath("//*[@name='MeetingName']"));
    public static final Target INPUT_NUMBER_MEET = Target.the("").located(By.xpath("//*[@name='MeetingNumber']"));
    public static final Target SELECT_LOCATION = Target.the("").located(By.xpath("//*[contains(text(),'Ubicación')]/../div"));
    public static final Target SELECT_LOCATION_UL = Target.the("").located(By.xpath("//*[@id='select2-results-7']"));
    public static final Target SELECT_UNIT = Target.the("").located(By.xpath("//*[contains(text(),'Unidad')]/../div"));
    public static final Target SELECT_UNIT_UL = Target.the("").located(By.xpath("//*[@id='select2-results-8']"));
    public static final Target SELECT_ORDER = Target.the("").located(By.xpath("(//*[contains(text(),'Organizado por')]/../div)[3]"));
    public static final Target SELECT_ORDER_UL = Target.the("").locatedBy("(//label[contains(text(),'Organizado por')])[3]/../../ul/li/div[contains(text(),'{0}')]");
    public static final Target SELECT_REPORTS = Target.the("").located(By.xpath("(//*[contains(text(),'Reportero')]/../div)[3]"));
    public static final Target SELECT_REPORTS_UL = Target.the("").locatedBy("(//label[contains(text(),'Reportero')])[3]/../../ul/li/div[contains(text(),'{0}')]");
    public static final Target SELECT_LIST_ASISTE = Target.the("").located(By.xpath("//*[@class='editor s-Serenity-Pro-Meeting-MeetingAttendeeEditor s-Pro-Meeting-MeetingAttendeeEditor s-MeetingAttendeeEditor s-DataGrid require-layout route-handler']/../div//div//a"));
    public static final Target SELECT_LIST_ASISTE_UL = Target.the("").locatedBy("//*[@class='select2-drop select2-with-searchbox select2-drop-active']//ul/li/div[contains(text(),'{0}')]");
    public static final Target BUTTON_SAVE = Target.the("").locatedBy("//*[@class='tool-button save-and-close-button icon-tool-button']");
    public static final Target LABEL_FINAL = Target.the("").locatedBy("(//*[contains(text(),'{0}')])[1]");




}
