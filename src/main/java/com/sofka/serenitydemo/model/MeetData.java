package com.sofka.serenitydemo.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.datatable.DataTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MeetData {
    private String nameMeeting;
    private String meetingType;
    private String meetingNumber;
    private String starDate;
    private String startHour;
    private String endDate;
    private String endHour;
    private String location;
    private String unit;
    private String organizaBy;
    private String reporter;
    private String invited1;
    private String invited2;
    private String invited3;

    public static List<MeetData>  setData(DataTable dataTable){
        List<MeetData> dates = new ArrayList<>();
        List<Map<String, String>> mapInfo = dataTable.asMaps();
        for (Map<String, String> map : mapInfo) {
            dates.add(new ObjectMapper().convertValue(map, MeetData.class));
        }
        return dates;
    }

    public String getNameMeeting() {
        return nameMeeting;
    }

    public String getMeetingType() {
        return meetingType;
    }

    public String getMeetingNumber() {
        return meetingNumber;
    }

    public String getStarDate() {
        return starDate;
    }

    public String getStartHour() {
        return startHour;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getEndHour() {
        return endHour;
    }

    public String getLocation() {
        return location;
    }

    public String getUnit() {
        return unit;
    }

    public String getOrganizaBy() {
        return organizaBy;
    }

    public String getReporter() {
        return reporter;
    }

    public String getInvited1() {
        return invited1;
    }

    public String getInvited2() {
        return invited2;
    }

    public String getInvited3() {
        return invited3;
    }
}
