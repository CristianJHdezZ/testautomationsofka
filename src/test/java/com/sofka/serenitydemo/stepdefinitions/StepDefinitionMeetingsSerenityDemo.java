package com.sofka.serenitydemo.stepdefinitions;

import com.sofka.serenitydemo.model.MeetData;
import com.sofka.serenitydemo.questions.LabelVisible;
import com.sofka.serenitydemo.tasks.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;



public class StepDefinitionMeetingsSerenityDemo {

    @Before
    public void setUp() {
        OnStage.setTheStage((new OnlineCast()));
    }

    @Given("it is required to enter the serenity demo page and log in with {string} and {string}")
    public void openSerenityDemo(String userName, String password) {
        OnStage.theActorCalled("miguel").wasAbleTo(
                OpenSerenity.thePage(),
                LoginSerenity.conCredenciales(userName, password)
        );

    }
    @When("a new business unit is created with the data {string} {string}")
    public void createBusinessUnit(String name, String parentUnit) {
        OnStage.theActorInTheSpotlight().attemptsTo(DiligenciarFormularioBusinessUnit.llenarFormulario(name,parentUnit));
    }
    @When("schedule a new meeting with the data")
    public void scheduleNewMeetingWithTheDatas(DataTable data) {
        OnStage.theActorInTheSpotlight().attemptsTo(ScheduleMeeting.llenarFormulario(MeetData.setData(data).get(0)));
    }
    @Then("validate that the meeting is created with name {string}")
    public void validateThatTheMeetingIsCreatedWithName(String meetingName) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(LabelVisible.inThe(meetingName))
        );
    }
}
