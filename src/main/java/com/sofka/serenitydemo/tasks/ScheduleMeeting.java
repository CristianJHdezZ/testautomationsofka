package com.sofka.serenitydemo.tasks;

import com.sofka.serenitydemo.interactions.SelectUnit;
import com.sofka.serenitydemo.model.MeetData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.sofka.serenitydemo.userinterface.MeetPage.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;


public class ScheduleMeeting implements Task {
    private final MeetData meetData;

    public ScheduleMeeting(MeetData meetData) {
        this.meetData = meetData;
    }

    public static ScheduleMeeting llenarFormulario(MeetData meetData) {
        return Tasks.instrumented(ScheduleMeeting.class,meetData);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BUTTON_MENU_MEET),
                Click.on(BUTTON_MENU_MEET_ITEM),
                Click.on(BUTTON_NEW_MEET),
                Enter.theValue(meetData.getNameMeeting()).into(INPUT_NAME_MEET),
                Enter.theValue(meetData.getMeetingNumber()).into(INPUT_NUMBER_MEET),
                Click.on(SELECT_LOCATION),
                SelectUnit.on(SELECT_LOCATION_UL,meetData.getLocation()),
                Click.on(SELECT_UNIT),
                SelectUnit.on(SELECT_UNIT_UL,meetData.getUnit())

        );

        actor.attemptsTo(
                Click.on(SELECT_ORDER),
                Click.on(SELECT_ORDER_UL.of(meetData.getOrganizaBy())),
                Click.on(SELECT_REPORTS),
                Click.on(SELECT_REPORTS_UL.of(meetData.getReporter())),
                Click.on(SELECT_LIST_ASISTE),
                Click.on(SELECT_LIST_ASISTE_UL.of(meetData.getInvited1())),
                Click.on(SELECT_LIST_ASISTE),
                Click.on(SELECT_LIST_ASISTE_UL.of(meetData.getInvited2())),
                Click.on(BUTTON_SAVE)
        );

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
