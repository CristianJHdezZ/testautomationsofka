package com.sofka.serenitydemo.tasks;

import com.sofka.serenitydemo.userinterface.LoginPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class Login implements Task {
//    static WebDriver driver;

    String userName;
    String password;


    public Login(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public static Login on(String userName, String password) {
        return Tasks.instrumented(Login.class, userName, password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {


        try {
            WebElement name = getDriver().findElement(By.xpath("//*[@name='Username']"));
            name.clear();
            name.sendKeys(userName);

//            Thread.sleep(10000);

            WebElement  pass =  getDriver().findElement(By.xpath("//*[@name='Password']"));
            pass.clear();
            pass.sendKeys(password);

            Serenity.setSessionVariable("resultado").to(true);

            actor.attemptsTo(Click.on(LoginPage.BUTTONSIGNIN));

            Thread.sleep(10000);

        }catch (Exception e){
            e.printStackTrace();
            Serenity.setSessionVariable("resultado").to(false);
        }
        finally {
            /*driver.close();*/
        }



    }
}
