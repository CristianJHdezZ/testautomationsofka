package com.sofka.serenitydemo.tasks;


import com.sofka.serenitydemo.interactions.SelectUnit;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.sofka.serenitydemo.userinterface.BusinessUnitPage.*;


public class DiligenciarFormularioBusinessUnit implements Task {
    private final String strName;
    private final String strParentUnit;

    public DiligenciarFormularioBusinessUnit(String strName, String strParentUnit) {
        this.strName = strName;
        this.strParentUnit = strParentUnit;
    }

    public static DiligenciarFormularioBusinessUnit llenarFormulario(String name, String parentUnit) {
        return Tasks.instrumented(DiligenciarFormularioBusinessUnit.class,name,parentUnit);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BUTTON_ORGANIZATION),
                Click.on(BUTTON_BUSINESS_UNIT),
                Click.on(BUTTON_ADD_BUSINESS),
                Enter.theValue(strName).into(INPUT_NAME),
                Click.on(INPUT_PRINCIPAL_UNIT),
                SelectUnit.on(INPUT_PRINCIPAL_UNIT_UL,strParentUnit),
                Click.on(BUTTON_SAVE)
        );
    }
}
