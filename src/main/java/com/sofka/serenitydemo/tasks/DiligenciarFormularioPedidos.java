package com.sofka.serenitydemo.tasks;


import com.sofka.serenitydemo.interactions.SelectUnit;
import com.sofka.serenitydemo.model.PedidoData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;


import static com.sofka.serenitydemo.userinterface.PedidosPage.*;


public class DiligenciarFormularioPedidos implements Task {

    private final PedidoData pedido;


    public DiligenciarFormularioPedidos(PedidoData pedido) {
        this.pedido = pedido;
    }

    public static DiligenciarFormularioPedidos llenarPedidos(PedidoData pedidos) {
        return net.serenitybdd.screenplay.Tasks.instrumented(DiligenciarFormularioPedidos.class, pedidos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(clienteTarget)
        );

        actor.attemptsTo(
                SelectUnit.on(SELECT_CLIENTE_UL, pedido.getCliente())
        );

        actor.attemptsTo(
                Enter.theValue(pedido.getfPedido()).into(fechaPedidoTarget)
        );

        actor.attemptsTo(
                Enter.theValue(pedido.getfEnvio()).into(fechaEnvioTarget)
        );

        actor.attemptsTo(
                Click.on(empleadoTarget)
        );

        actor.attemptsTo(
                SelectUnit.on(SELECT_EMPLEADO_UL, pedido.getEmpleado())
        );

        actor.attemptsTo(
                Click.on(productosButtonTarget)
        );
        actor.remember("Empleado",pedido.getEmpleado());
    }
}