package com.sofka.serenitydemo.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.sofka.serenitydemo.userinterface.MeetPage.*;


public class LabelVisible implements Question<Boolean> {
    private final String strLabel;

    public LabelVisible(String strLabel) {
        this.strLabel = strLabel;

    }

    public static LabelVisible inThe(String strLabel) {
        return new LabelVisible(strLabel);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return LABEL_FINAL.of(strLabel).resolveFor(actor).isDisplayed();
    }
}
