package com.sofka.serenitydemo.runners;



import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/serenity_demo_conferencias.feature",
        tags = "@Regresion",
        glue = "com.sofka.serenitydemo.stepdefinitions",
        snippets = CucumberOptions.SnippetType.CAMELCASE )

public class RunnerMeetingsSerenityDemo {
}
