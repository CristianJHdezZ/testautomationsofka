package com.sofka.serenitydemo.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class BusinessUnitPage {
    public static final Target BUTTON_ORGANIZATION = Target.the("Button Menu Item Organizacion")
            .located(By.xpath("//*[@id='nav_menu1_3']/li[1]/a/span"));

    public static final Target BUTTON_BUSINESS_UNIT = Target.the("Button Menu Item BusinessUnit")
            .located(By.xpath("//*[@id='nav_menu1_3_1']/li[1]/a/span"));

    public static final Target BUTTON_ADD_BUSINESS = Target.the("")
            .located(By.xpath("//*[@id='GridDiv']/div[2]/div[2]/div[1]/span"));

    public static final Target INPUT_NAME = Target.the("").located(By.xpath("//*[@id='Serenity_Pro_Organization_BusinessUnitDialog3_Name']"));
    public static final Target INPUT_PRINCIPAL_UNIT = Target.the("").located(By.xpath("//*[@id='select2-chosen-1']"));
    public static final Target INPUT_PRINCIPAL_UNIT_UL = Target.the("").located(By.xpath("//*[@id='select2-results-1']"));
    public static final Target BUTTON_SAVE = Target.the("").located(By.xpath("//*[@id='Serenity_Pro_Organization_BusinessUnitDialog3_Toolbar']/div/div[1]"));



}
